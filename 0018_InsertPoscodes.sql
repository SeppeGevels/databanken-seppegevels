use ModernWays;

insert into postcodes
	(
		postcodes.Code,
        postcodes.Plaats,
        postcodes.Localite,
        postcodes.Provincie,
        postcodes.Province
	)
    
    value
    ('2800', 'Mechelen', 'Malines', 'Antwerpen', 'Anvers'),
    ('3000', 'Leuven', 'Louvain', 'Vlaams-Brabant', 'Brabant flamand');