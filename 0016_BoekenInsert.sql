use ModernWays;

insert into Boeken 
	(
	boeken.Id,
	boeken.Voornaam,
    boeken.Familienaam,
    boeken.Titel,
    boeken.Stad,
    boeken.Uitgeverij,
    boeken.Verschijningsdatum,
    boeken.Herdruk,
    boeken.Commentaar,
    boeken.Categorie
	)

    values
    ('1','Samuel', 'Ijsseling', 'Heidegger. Denken en Zijn. Geven en Danken', 'Amsterdam','', '2014', '', 'Nog te lezen', 'Filosofie'),
    ('2','Jacob', 'Van Sluis', 'Lees wijzer bij Zijn en Tijd','', 'Budel', '1998', '', 'Goed boek', 'Filosofie');
    